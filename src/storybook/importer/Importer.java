/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.importer;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.hibernate.Session;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import storybook.SbApp;
import storybook.controller.BookController;
import storybook.model.BookModel;
import storybook.model.EntityUtil;
import storybook.model.handler.AbstractEntityHandler;
import storybook.model.hbn.dao.SbGenericDAOImpl;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.toolkit.I18N;
import storybook.ui.MainFrame;
import storybook.ui.dialog.DlgException;

/**
 *
 * @author favdb
 */
public class Importer {

	public String fileName;
	private final String fileExtension;
	private boolean fileOpened=false;
	private Document document;
	public String rootName;
	public Element rootNode;
	private DocumentBuilder documentBuilder;
	BookModel bookModel;
	private final MainFrame			mainFrame;
	private final BookController	currentCtrl;
	private SbGenericDAOImpl<?, ?>	currentDao;
	private String msgCheck;
	
	public Importer(String n, MainFrame m) {
		fileName=n;
		fileExtension=fileName.substring(fileName.lastIndexOf("."));
		mainFrame=m;
		currentCtrl=mainFrame.getBookController();
		SbApp.trace("fileName="+fileName+", ext="+fileExtension);
	}
	
	public boolean open() {
		boolean rc=fileName.endsWith(".xml")?openXml():openSb();
		return(rc);
	}
	
	public boolean isOpened() {
		return(fileOpened);
	}
	
	public String getType() {
		return(fileExtension);
	}
	
	public boolean isXml() {
		return(fileExtension.equals(".xml"));
	}
	
	public void close() {
		if (fileName.endsWith(".xml")) {
			closeXml();
		} else {
			closeSb();
		}
	}

	private boolean openXml() {
		fileOpened=false;
		documentBuilder = null;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			DlgException.showDialog("", new Exception(I18N.getMsg("dlg.import.open.docbuilder")));
			System.err.println("DocumentBuilder error:\n"+ex.getLocalizedMessage());
			return(false);
		}
		document = readDom();
		if (document==null) {
			rootNode=null;
			return(false);
		}
		rootNode=document.getDocumentElement();
		Element n=(Element)rootNode.getElementsByTagName("book").item(0);
		if (n!=null) {
			rootName = n.getFirstChild().getNodeName();
		}
		fileOpened=true;
		return(true);
	}
	
	public Document readDom() {
		SbApp.trace("Trying to open "+fileName);
		Document rc=null;
		try {
			rc=documentBuilder.parse(new File(fileName));
		} catch (SAXException e) {
			DlgException.showDialog("", new Exception(I18N.getMsg("dlg.import.open.parsing")));
			System.err.println("Parsing error for " + fileName + "\n" + e.getMessage());
		} catch (IOException e) {
			DlgException.showDialog("", new Exception(I18N.getMsg("dlg.import.open.io")));
			System.err.println("I/O error for " + fileName + "\n" + e.getMessage());
		}
		return(rc);
	}

	public void closeXml() {
		if (fileOpened) {
			fileOpened=false;
			document=null;
			documentBuilder=null;
		}
	}

	private boolean openSb() {
		boolean rc=false;
		if (rc) fileOpened=true;
		return(rc);
	}

	private void closeSb() {
		if (isOpened()) {
			
		}
		fileOpened=false;
	}

	/* return true if entity exists and not force */
	public boolean write(AbstractEntity entity, boolean force) {
		System.out.println("Importer.write("+EntityUtil.getEntityName(entity)+") "+(force?"force update":""));
		if (!check(entity).isEmpty()) {
			if (!force) return(true);
			AbstractEntity n=ImportUtil.updateEntity(entity,(AbstractEntity) currentDao.find(entity.getId()));
			currentCtrl.updateEntity(n);
		}
		else currentCtrl.newEntity(entity);
		return(false);
	}

	public boolean writeAll(List<AbstractEntity> entities, boolean force) {
		if (entities.isEmpty()) return(false);
		System.out.println("Importer.writeAll("+EntityUtil.getEntityTitle(entities.get(0))+") "+(force?"force update":""));
		boolean rc=false;
		AbstractEntityHandler handler = EntityUtil.getEntityHandler(mainFrame, entities.get(0));
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		currentDao = handler.createDAO();
		currentDao.setSession(session);
		for (AbstractEntity entity:entities) {
			if (write(entity, force)) {
				JOptionPane.showMessageDialog(null,
					I18N.getMsg("dlg.import.notok",EntityUtil.getEntityTitle(entity)+" : "+EntityUtil.getEntityName(entity)),
					I18N.getMsg("msg.common.import"),
					JOptionPane.ERROR_MESSAGE);
				rc=true;
				break;
			}
		}
		// commit inutile car il se fait lors de chaque ecriture
		return(rc);
	}
	
	/* check if the entity exists already, return String if yes */
	String check(AbstractEntity entity) {
		System.out.println("Importer.check("+EntityUtil.getEntityName(entity)+")");
		String rc="";
		if (currentDao.find(entity.getId())!=null) {
			rc=" exists";
		}
		return(rc);
	}
	
	String checkAll(List<AbstractEntity> entities) {
		if (entities.isEmpty()) return("");
		System.out.println("Importer.checkAll("+EntityUtil.getEntityClass(entities.get(0)).getName()+")");
		msgCheck="";
		//TODO Importer.check(entity) à écrire
		AbstractEntityHandler handler = EntityUtil.getEntityHandler(mainFrame, entities.get(0));
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		currentDao = handler.createDAO();
		currentDao.setSession(session);
		for (AbstractEntity entity:entities) {
			String msg=check(entity);
			if (!msg.isEmpty()) {
				msgCheck+=EntityUtil.getEntityName(entity)+msg+"\n";
			}
		}
		model.commit();
		return(msgCheck);
	}
	
	List<AbstractEntity> list(String tobj) {
		return(ImportUtil.list(this,tobj));
	}

}
