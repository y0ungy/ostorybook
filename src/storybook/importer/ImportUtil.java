/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.importer;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import storybook.SbApp;
import storybook.model.hbn.dao.CategoryDAOImpl;
import storybook.model.hbn.dao.ChapterDAOImpl;
import storybook.model.hbn.dao.GenderDAOImpl;
import storybook.model.hbn.dao.ItemDAOImpl;
import storybook.model.hbn.dao.ItemLinkDAOImpl;
import storybook.model.hbn.dao.LocationDAOImpl;
import storybook.model.hbn.dao.PartDAOImpl;
import storybook.model.hbn.dao.PersonDAOImpl;
import storybook.model.hbn.dao.SbGenericDAOImpl;
import storybook.model.hbn.dao.SceneDAOImpl;
import storybook.model.hbn.dao.StrandDAOImpl;
import storybook.model.hbn.dao.TagDAOImpl;
import storybook.model.hbn.dao.TagLinkDAOImpl;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Category;
import storybook.model.hbn.entity.Chapter;
import storybook.model.hbn.entity.Gender;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.ItemLink;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Part;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Scene;
import storybook.model.hbn.entity.Strand;
import storybook.model.hbn.entity.Tag;
import storybook.model.hbn.entity.TagLink;

/**
 *
 * @author favdb
 */
class ImportUtil {

	static List<AbstractEntity> list(Importer imp, String tobj) {
		SbApp.trace("ImportEntities.list("+imp.fileName+","+tobj+")");
		if (!imp.isOpened()) {
			System.err.println("error Importer is not opened");
			return(new ArrayList<>());
		}
		if (imp.isXml()) {
			return(listXml(imp, tobj));
		}
		return(listH2(imp, tobj));
	}

	static List<AbstractEntity> listXml(Importer imp, String tobj) {
		SbApp.trace("ImportEntities.Xml("+imp.fileName+","+tobj+")");
		List<AbstractEntity> entities=new ArrayList<>();
		NodeList nodes=imp.rootNode.getElementsByTagName(tobj);
		for(int i=0; i<nodes.getLength();i++) {
			Node n=nodes.item(i);
			if (!n.getParentNode().equals(imp.rootNode)) continue;
			switch(tobj) {
				case "strand":
					Strand strand=Strand.fromXml(n);
					if (strand.getId()>1) entities.add(strand);
					break;
				case "part":
					Part part=Part.fromXml(n);
					if (part.getId()>1) entities.add(part);
					break;
				case "chapter":
					entities.add(Chapter.fromXml(n));
					break;
				case "scene":
					entities.add(Scene.fromXml(n));
					break;
				case "person":
					entities.add(Person.fromXml(n));
					break;
				case "gender":
					Gender gender=Gender.fromXml(n);
					if (gender.getId()>2) entities.add(gender);
					break;
				case "category":
					Category category=Category.fromXml(n);
					if (category.getId()>2) entities.add(category);
					break;
				case "location":
					entities.add(Location.fromXml(n));
					break;
				case "item":
					entities.add(Item.fromXml(n));
					break;
				case "itemlink":
					entities.add(ItemLink.fromXml(n));
					break;
				case "tag":
					entities.add(Tag.fromXml(n));
					break;
				case "taglink":
					entities.add(TagLink.fromXml(n));
					break;
			}
		}
		return(entities);
	}
	@SuppressWarnings("unchecked")
	static List<AbstractEntity> listH2(Importer imp, String tobj) {
		Session session = imp.bookModel.beginTransaction();
		SbGenericDAOImpl<?, ?> dao=null;
		List<AbstractEntity> entities = new ArrayList<>();
		switch(tobj) {
			case "strand":		dao = new StrandDAOImpl(session); break;
			case "part":		dao = new PartDAOImpl(session); break;
			case "chapter":		dao = new ChapterDAOImpl(session); break;
			case "scene":		dao = new SceneDAOImpl(session); break;
			case "person":		dao = new PersonDAOImpl(session); break;
			case "category":	dao = new CategoryDAOImpl(session); break;
			case "gender":		dao = new GenderDAOImpl(session); break;
			case "location":	dao = new LocationDAOImpl(session); break;
			case "item":		dao = new ItemDAOImpl(session); break;
			case "itemlink":	dao = new ItemLinkDAOImpl(session); break;
			case "tag":			dao = new TagDAOImpl(session); break;
			case "taglink":		dao = new TagLinkDAOImpl(session); break;
		}
		if (dao!=null) {
			entities = (List<AbstractEntity>)dao.findAll();
			imp.bookModel.commit();
		}
		return(entities);
	}

	static AbstractEntity updateEntity(AbstractEntity newEntity, AbstractEntity oldEntity) {
		if (newEntity instanceof Strand) {
			return(updateStrand((Strand)newEntity,(Strand)oldEntity));
		} else if (newEntity instanceof Part) {
			return(updatePart((Part)newEntity,(Part)oldEntity));
		} else if (newEntity instanceof Chapter) {
			return(updateChapter((Chapter)newEntity,(Chapter)oldEntity));
		} else if (newEntity instanceof Scene) {
			return(updateScene((Scene)newEntity,(Scene)oldEntity));
		} else if (newEntity instanceof Category) {
			return(updateCategory((Category)newEntity,(Category)oldEntity));
		} else if (newEntity instanceof Gender) {
			return(updateGender((Gender)newEntity,(Gender)oldEntity));
		} else if (newEntity instanceof Person) {
			return(updatePerson((Person)newEntity,(Person)oldEntity));
		} else if (newEntity instanceof Location) {
			return(updateLocation((Location)newEntity,(Location)oldEntity));
		}
		return(newEntity);
	}

	private static AbstractEntity updateStrand(Strand newEntity, Strand oldEntity) {
		Strand entity=newEntity;
		// as is, no link to update
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updatePart(Part newEntity, Part oldEntity) {
		Part entity=newEntity;
		// creationTime, doneTime, objectiveTime, objectiveChars not modfied
		entity.setCreationTime(oldEntity.getCreationTime());
		entity.setDoneTime(oldEntity.getDoneTime());
		entity.setObjectiveChars(oldEntity.getObjectiveChars());
		entity.setObjectiveTime(oldEntity.getObjectiveTime());
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updateChapter(Chapter newEntity, Chapter oldEntity) {
		Chapter entity=newEntity;
		// creationTime, doneTime, objectiveTime, objectiveChars not modfied
		entity.setCreationTime(oldEntity.getCreationTime());
		entity.setDoneTime(oldEntity.getDoneTime());
		entity.setObjectiveChars(oldEntity.getObjectiveChars());
		entity.setObjectiveTime(oldEntity.getObjectiveTime());
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updateScene(Scene newEntity, Scene oldEntity) {
		Scene entity=newEntity;
		// chapter, strand, persons, items not modified
		entity.setStrand(oldEntity.getStrand());
		entity.setChapter(oldEntity.getChapter());
		entity.setPersons(oldEntity.getPersons());
		entity.setItems(oldEntity.getItems());
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updateCategory(Category newEntity, Category oldEntity) {
		Category entity=newEntity;
		// sup not modified
		entity.setSup(oldEntity.getSup());
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updateGender(Gender newEntity, Gender oldEntity) {
		Gender entity=newEntity;
		// as is, no link to update
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updatePerson(Person newEntity, Person oldEntity) {
		Person entity=newEntity;
		// gender, category, attributes not modified
		entity.setGender(oldEntity.getGender());
		entity.setCategory(oldEntity.getCategory());
		entity.setAttributes(oldEntity.getAttributes());
		return((AbstractEntity)entity);
	}
	
	private static AbstractEntity updateLocation(Location newEntity, Location oldEntity) {
		Location entity=newEntity;
		// site not modified
		entity.setSite(oldEntity.getSite());
		return((AbstractEntity)entity);
	}
	
	//TOTO updateItem, updateTag, updateIdea, updateMemo, updateRelationship, updateTagLink, update ItemLink
	
}
