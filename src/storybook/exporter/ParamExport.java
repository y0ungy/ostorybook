/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storybook.exporter;

import storybook.SbConstants;
import storybook.SbPref;
import storybook.toolkit.BookUtil;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class ParamExport {

	public MainFrame mainFrame;
	String format="xml";
	public String csvQuote="\"";
	public String csvComma;
	public boolean txtTab;
	public String txtSeparator;
	public boolean htmlUseCss;
	public String htmlCssFile;
	public boolean isExportChapterNumbers;
	public boolean isExportChapterNumbersRoman;
	public boolean isExportChapterTitles;
	public boolean isExportChapterDatesLocs;
	public boolean isExportSceneTitles;
	public boolean isExportSceneSeparator;
	public boolean htmlBookMulti;
	public String pdfPageSize;
	public boolean pdfLandscape;
	public boolean htmlNavImage=false;

	public ParamExport(MainFrame m) {
		mainFrame = m;
		init();
	}

	void init() {
		format=mainFrame.getPref().getString(SbPref.Key.EXPORT_PREF, "010");
		csvQuote = mainFrame.getPref().getString(SbPref.Key.EXPORT_CSV_QUOTE, "'");
		csvComma = mainFrame.getPref().getString(SbPref.Key.EXPORT_CSV_COMMA, ";");
		txtTab = mainFrame.getPref().getBoolean(SbPref.Key.EXPORT_TXT_TAB, true);
		if (!txtTab)
			txtSeparator = mainFrame.getPref().getString(SbPref.Key.EXPORT_TXT_SEPARATOR, "");
		htmlUseCss = BookUtil
				.get(mainFrame, SbConstants.BookKey.HTML_USE_CSS, false).getBooleanValue();
		htmlNavImage = BookUtil
				.get(mainFrame, SbConstants.BookKey.HTML_NAV_IMAGE, false).getBooleanValue();
		if (htmlUseCss)
			htmlCssFile = BookUtil
					.get(mainFrame, SbConstants.BookKey.HTML_CSS_FILE, "").getStringValue();
		else
			htmlCssFile = "";
		htmlBookMulti = BookUtil
				.get(mainFrame, SbConstants.BookKey.HTML_BOOK_MULTI, false).getBooleanValue();
		isExportChapterNumbers = BookUtil
				.get(mainFrame, SbConstants.BookKey.EXPORT_CHAPTER_NUMBERS, false).getBooleanValue();
		isExportChapterNumbersRoman = BookUtil
				.get(mainFrame, SbConstants.BookKey.EXPORT_ROMAN_NUMERALS, false).getBooleanValue();
		isExportChapterTitles = BookUtil
				.get(mainFrame, SbConstants.BookKey.EXPORT_CHAPTER_TITLES, false).getBooleanValue();
		isExportChapterDatesLocs = BookUtil
				.get(mainFrame, SbConstants.BookKey.EXPORT_CHAPTER_DATES_LOCATIONS, false).getBooleanValue();
		isExportSceneTitles = BookUtil
				.get(mainFrame, SbConstants.BookKey.EXPORT_SCENE_TITLES, false).getBooleanValue();
		isExportSceneSeparator = BookUtil
				.get(mainFrame, SbConstants.BookKey.EXPORT_SCENE_SEPARATOR, false).getBooleanValue();
		pdfPageSize = BookUtil
				.get(mainFrame, SbConstants.BookKey.PDF_PAGE_SIZE, "A4").getStringValue();
		pdfLandscape = BookUtil
				.get(mainFrame, SbConstants.BookKey.PDF_LANDSCAPE, false).getBooleanValue();
	}

	void save() {
		mainFrame.getPref().setString(SbPref.Key.EXPORT_CSV_QUOTE, csvQuote);
		mainFrame.getPref().setString(SbPref.Key.EXPORT_CSV_COMMA, csvComma);
		mainFrame.getPref().setBoolean(SbPref.Key.EXPORT_TXT_TAB, txtTab);
		mainFrame.getPref().setString(SbPref.Key.EXPORT_TXT_SEPARATOR, txtSeparator);
		BookUtil.store(mainFrame, SbConstants.BookKey.HTML_USE_CSS.toString(), htmlUseCss);
		BookUtil.store(mainFrame, SbConstants.BookKey.HTML_NAV_IMAGE.toString(), htmlNavImage);
		BookUtil.store(mainFrame, SbConstants.BookKey.HTML_CSS_FILE.toString(), htmlCssFile);
		BookUtil.store(mainFrame, SbConstants.BookKey.HTML_BOOK_MULTI.toString(), htmlBookMulti);
		BookUtil.store(mainFrame, SbConstants.BookKey.EXPORT_CHAPTER_NUMBERS.toString(), isExportChapterNumbers);
		BookUtil.store(mainFrame, SbConstants.BookKey.EXPORT_ROMAN_NUMERALS.toString(), isExportChapterNumbersRoman);
		BookUtil.store(mainFrame, SbConstants.BookKey.EXPORT_CHAPTER_TITLES.toString(), isExportChapterTitles);
		BookUtil.store(mainFrame, SbConstants.BookKey.EXPORT_CHAPTER_DATES_LOCATIONS.toString(), isExportChapterDatesLocs);
		BookUtil.store(mainFrame, SbConstants.BookKey.EXPORT_SCENE_TITLES.toString(), isExportSceneTitles);
		BookUtil.store(mainFrame, SbConstants.BookKey.EXPORT_SCENE_SEPARATOR.toString(), isExportSceneSeparator);
		BookUtil.store(mainFrame, SbConstants.BookKey.PDF_PAGE_SIZE.toString(), pdfPageSize);
		BookUtil.store(mainFrame, SbConstants.BookKey.PDF_LANDSCAPE.toString(), pdfLandscape);
	}
	
}
