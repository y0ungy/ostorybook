/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package storybook.exporter;

import java.awt.HeadlessException;
import storybook.model.BookModel;
import storybook.model.hbn.dao.ChapterDAOImpl;
import storybook.model.hbn.dao.PartDAOImpl;
import storybook.model.hbn.dao.SceneDAOImpl;
import storybook.model.hbn.entity.Chapter;
import storybook.model.hbn.entity.Part;
import storybook.model.hbn.entity.Scene;
import storybook.toolkit.DateUtil;
import storybook.toolkit.BookUtil;
import storybook.toolkit.I18N;
import storybook.toolkit.LangUtil;
import storybook.ui.MainFrame;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import storybook.SbApp;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.toolkit.IOUtil;
import storybook.toolkit.TextTransfer;
import storybook.toolkit.html.HtmlUtil;

/*
 * BookExporter : main class for export the text of the book
 * Only export :
 * - title of Part : if more than one part
 * - title of chapter : optional, with optional number
 * - title of scene : optional
 * - didascalie : optional
 * - text of scene
 * @author favdb
 */
public class BookExporter extends AbstractExporter {

	boolean isUseHtmlScenes=false;
	boolean isExportChapterNumbers=false;
	boolean isExportRomanNumerals=false;
	boolean isExportChapterTitles=false;
	boolean isExportChapterDatLoc=false;
	boolean isExportSceneTitle=false;
	boolean isExportSceneDidascalie=false;
	boolean isExportSceneSeparator=false;
	boolean isExportPartTitles=false;
	boolean tHtml = true;
	boolean isBookHtmlMulti=false;

	private boolean exportForOpenOffice = false;
	private boolean exportOnlyCurrentPart = false;
	private boolean exportTableOfContentsLink = false;
	private HashSet<Long> strandIdsToExport = null;
	private final String bH1 = "<h1>", eH1 = "</h1>",
			bH2 = "<h2>", eH2 = "</h2>",
			bH3 = "<h3>", eH3 = "</h3>",
			bH4 = "<h4>", eH4 = "</h4>",
			bTx = "<p>", eTx = "</p";

	public BookExporter(MainFrame m) {
		super(m);
		setFileName(m.getDbFile().getName());
		getParam();
		SbApp.trace("BookExporter(" + m.getName() + ")");
	}

	public boolean exportToClipboard() {
		SbApp.trace("BookExporter.exportToClipboard()");
		try {
			tHtml=false;
			exportForOpenOffice = false;
			StringBuffer str = getContent();
			TextTransfer tf=new TextTransfer();
			tf.setClipboardContents(str.toString());
			JOptionPane.showMessageDialog(this.mainFrame,
					I18N.getMsg("msg.book.copy.confirmation"),
					I18N.getMsg("msg.copied.title"), 1);
		} catch (HeadlessException exc) {
			return false;
		}
		return true;
	}

	private void getParam() {
		isUseHtmlScenes = BookUtil.isUseHtmlScenes(mainFrame);
		isExportChapterNumbers = BookUtil.isExportChapterNumbers(mainFrame);
		isExportRomanNumerals = BookUtil.isExportRomanNumerals(mainFrame);
		isExportChapterTitles = BookUtil.isExportChapterTitles(mainFrame);
		isExportChapterDatLoc = BookUtil.isExportChapterDatesLocations(mainFrame);
		isExportSceneTitle = BookUtil.isExportSceneTitle(mainFrame);
		isExportSceneDidascalie = BookUtil.isExportSceneDidascalie(mainFrame);
		isExportSceneSeparator = BookUtil.isExportSceneSeparator(mainFrame);
		isExportPartTitles = BookUtil.isExportPartTitles(mainFrame);

		tHtml = !((!isUseHtmlScenes) && (exportForOpenOffice == true)); //buf.append(getHtmlHead());
		isBookHtmlMulti = BookUtil.isExportBookHtmlMulti(mainFrame);
	}

	@Override
	public StringBuffer getContent() {
		// warning : getContent ne retourne que le contenu du body en mode HTML
		SbApp.trace("BookExporter.getContent()");
		Part Part1 = mainFrame.getCurrentPart();
		StringBuffer buf = new StringBuffer();
		try {
			BookModel model = mainFrame.getBookModel();
			Session session = model.beginTransaction();
			PartDAOImpl PartDAO = new PartDAOImpl(session);
			ChapterDAOImpl ChapterDAO = new ChapterDAOImpl(session);
			SceneDAOImpl SceneDAO = new SceneDAOImpl(session);
			List<Part> listParts;
			if (exportOnlyCurrentPart) {
				listParts = new ArrayList<>();
				listParts.add(Part1);
			} else {
				listParts = PartDAO.findAll();
			}
			List<Chapter> chapters = ChapterDAO.findAll();
			List<Scene> scenes = SceneDAO.findAll();
			if (tHtml) {// export en HTML
				for (Part part : listParts) {
					if (listParts.size()>1) buf.append(getPartAsHtml(part));
					for (Chapter chapter : chapters) {
						if (chapter.getPart().equals(part)) {
							buf.append(getChapterAsHtml(chapter, ChapterDAO));
							for (Scene scene : scenes) {
								if (scene.getChapter()==null) continue;
								if (scene.getChapter().equals(chapter)) buf.append(getSceneAsHtml(scene));
							}
						}
					}
				} // fin export HTML
			} else {// export en TXT
				for (Part part : listParts) {
					if (listParts.size()>1) buf.append(getPartAsTxt(part));
					for (Chapter chapter : chapters) {
						if (chapter.getPart().equals(part)) {
							buf.append(getChapterAsTxt(chapter, ChapterDAO));
							for (Scene scene : scenes) {
								if (scene.getChapter()==null) continue;
								if (scene.getChapter().equals(chapter)) buf.append(getSceneAsTxt(scene));
							}
						}
					}
				}
			} // fin export TXT
			model.commit();
		} catch (Exception exc) {
			SbApp.error("BookExport.getContent()", exc);
		}
		SbApp.trace("getContent returns bufsize=" + buf.length());
		return buf;
	}

	public boolean isExportOnlyCurrentPart() {
		return exportOnlyCurrentPart;
	}

	public void setExportOnlyCurrentPart(boolean b) {
		exportOnlyCurrentPart = b;
	}

	public void setExportSceneTitle(boolean b) {
		isExportSceneTitle = b;
	}

	public void setExportSceneDidascalie(boolean b) {
		isExportSceneDidascalie = b;
	}

	public boolean isExportTableOfContentsLink() {
		return exportTableOfContentsLink;
	}

	public void setExportTableOfContentsLink(boolean b) {
		exportTableOfContentsLink = b;
	}

	public HashSet<Long> getStrandIdsToExport() {
		return strandIdsToExport;
	}

	public void setStrandIdsToExport(HashSet<Long> p) {
		strandIdsToExport = p;
	}

	public void setExportToTxt() {
		tHtml=false;
	}
	
	public boolean isExportForOpenOffice() {
		return exportForOpenOffice;
	}

	public void setExportForOpenOffice(boolean b) {
		exportForOpenOffice = b;
	}

	public String getPartAsTxt(Part part) {
		SbApp.trace("BookExporter.getPartAsTxt(...)");
		String buf = "";
		if (isExportPartTitles) {
			buf += I18N.getMsg("msg.common.part") + ": " + part.getNumber();
		}
		return (buf);
	}

	@SuppressWarnings("unchecked")
	public String getChapterAsTxt(Chapter chapter, ChapterDAOImpl ChapterDAO) {
		SbApp.trace("BookExporter.getChapterAsTxt(...)");
		String buf = "";
		//buf += chapter.getChapternoStr() + "\n";
		if (isExportChapterNumbers) {
			if (isExportRomanNumerals) {
				buf += (String) LangUtil.intToRoman(chapter.getChapterno())+" ";
			} else {
				buf += chapter.getChapternoStr()+" ";
			}
		}
		if (isExportChapterTitles) {
			buf += chapter.getTitle();
		}
		buf += "\n";
		if (isExportChapterDatLoc) {
			buf += DateUtil.getNiceDates((List) ChapterDAO.findDates(chapter));
			if (!((List) ChapterDAO.findLocations(chapter)).isEmpty()) {
				buf += ": " + StringUtils.join((Iterable) ChapterDAO.findLocations(chapter), ", ");
			}
		}
		return (buf);
	}

	public String getSceneAsTxt(Scene scene) {
		SbApp.trace("BookExporter.getSceneAsTxt(...)");
		String buf = "";
		boolean bx = true;
		if (strandIdsToExport != null) {
			long l = scene.getStrand().getId();
			if (!strandIdsToExport.contains(l)) {
				return ("");
			}
		}
		if (bx) {
			if (!scene.getInformative()) {
				if (isExportSceneTitle) {
					buf += scene.getTitle()+"\n";
				}
				if (isExportSceneDidascalie) {
					buf += getDidascalie(scene)+"\n";
				}
				String str = HtmlUtil.htmlToText(scene.getText(),true);
				buf += str + "\n";
			}
		}
		return (buf);
	}

	public String getPartAsHtml(Part part) {
		SbApp.trace("BookExporter.getPartAsHtml(...)");
		String buf = "";
		if (isExportPartTitles) {
			buf = bH1 + I18N.getMsg("msg.common.part") + ": " + part.getNumber() + eH1;
		}
		return (buf);
	}

	@SuppressWarnings("unchecked")
	public String getChapterAsHtml(Chapter chapter, ChapterDAOImpl ChapterDAO) {
		SbApp.trace("BookExporter.getChapterAsHtml(...)");
		String buf="";
		buf += bH2;
		if (tHtml) buf=bH2+"<a name='" + chapter.getChapternoStr() + "'>";
		if (isExportChapterNumbers) {
			if (isExportRomanNumerals) {
				buf += (String) LangUtil.intToRoman(chapter.getChapterno());
			} else {
				buf += Integer.toString(chapter.getChapterno());
			}
			if (isExportChapterTitles) {
				buf += ": " + chapter.getTitle();
			}
		} else {
			if (isExportChapterTitles) {
				buf += chapter.getTitle();
			}
		}
		if (tHtml) buf += eH2 + "</a>\n";
		else buf+="\n";
		if (isExportChapterDatLoc) {
			if (tHtml) buf += bH3;
			buf += DateUtil.getNiceDates((List) ChapterDAO.findDates(chapter));
			if (!((List) ChapterDAO.findLocations(chapter)).isEmpty()) {
				buf += ": " + StringUtils.join((Iterable) ChapterDAO.findLocations(chapter), ", ");
			}
			if (tHtml) buf += eH3;
		}
		return (buf);
	}

	public String getChapterAsHtml(Chapter chapter) { // strict chapter without dates and locations with scenes
		SbApp.trace("BookExporter.getChapterAsHtml(...) without dao");
		String buf = "";
		if (tHtml) buf += "<a name='" + chapter.getChapternoStr() + "'>"+bH2;
		if (isExportChapterNumbers) {
			if (isExportRomanNumerals) {
				buf += (String) LangUtil.intToRoman(chapter.getChapterno());
			} else {
				buf += Integer.toString(chapter.getChapterno());
			}
			if (isExportChapterTitles) {
				buf += ": " + chapter.getTitle();
			}
		} else {
			if (isExportChapterTitles) {
				buf += chapter.getTitle();
			}
		}
		if (tHtml) buf += eH2 + "</a>\n";
		else buf+="\n";
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		SceneDAOImpl SceneDAO = new SceneDAOImpl(session);
		List<Scene> scenes = SceneDAO.findByChapter(chapter);
		for (Scene scene : scenes) {
			buf += getSceneAsHtml(scene);
		}
		model.commit();
		return (buf);
	}
	
	// get a Scene and replace all absolute path with a relative one
	public String getSceneAsHtml(Scene scene, String path) {
		SbApp.trace("BookExporter.getSceneAsHtml(...)");
		String str=getSceneAsHtml(scene);
		return(IOUtil.convertToRrelativePath(str, path));
	}

	public String getSceneAsHtml(Scene scene) {
		SbApp.trace("BookExporter.getSceneAsHtml(...) without path");
		String buf = "";
		if (strandIdsToExport != null) {
			long l = scene.getStrand().getId();
			if (!strandIdsToExport.contains(l)) {
				return ("");
			}
		}
		if (!scene.getInformative()) {
			if (isExportSceneTitle) {
				String t=scene.getTitle();
					t=HtmlUtil.getHtag(t);
				buf += t;
			}
			if (isExportSceneDidascalie) {
				buf+=getDidascalie(scene);
			}
			buf += scene.getText() + "\n";
			if (isExportSceneSeparator) {
				buf += "<p style=\"text-align:center\">.oOo.</p>\n";
			}
		}
		if (exportTableOfContentsLink) {
			buf += "<p style='font-size:8px;text-align:left;'><a href='#toc'>"
					+ I18N.getMsg("msg.table.of.contents")
					+ "</a></p>\n";
		}
		return (buf);
	}
	
	public String getDidascalie(Scene scene) {
		SbApp.trace("BookExporter.getDidascalie(...)");
		String rc="";
		if (isExportSceneDidascalie) {
			// personnages
			if (!scene.getPersons().isEmpty()) {
				if (tHtml) {
					rc+="<i><b>"+I18N.getMsg("msg.common.persons")+"</b> : ";
				} else rc+=I18N.getMsg("msg.common.persons")+": ";
				for (Person p: scene.getPersons()) {
					rc+=p.getFullName()+", ";
				}
				rc=rc.substring(0, rc.length()-2);
				if (tHtml) {
					rc+="</i><br>";
				}
				rc+="\n";
			}
			// lieux
			if (!scene.getLocations().isEmpty()) {
				if (tHtml) {
					rc+="<i><b>"+I18N.getMsg("msg.common.locations")+"</b> : ";
				} else rc+=I18N.getMsg("msg.common.locations")+": ";
				for (Location p: scene.getLocations()) {
					rc+=p.getFullName()+", ";
				}
				rc=rc.substring(0, rc.length()-2);
				if (tHtml) {
					rc+="</i><br>";
				}
				rc+="\n";
			}
			// items
			if (!scene.getItems().isEmpty()) {
				if (tHtml) {
					rc+="<i><b>"+I18N.getMsg("msg.common.items")+"</b> : ";
				} else rc+=I18N.getMsg("msg.common.items")+": ";
				for (Item p: scene.getItems()) {
					rc+=p.getName()+", ";
				}
				rc=rc.substring(0, rc.length()-2);
				if (tHtml) {
					rc+="</i><br>";
				}
				rc+="\n";
			}
			if (tHtml) {
				if (!rc.isEmpty()) rc="<p style=\"text-align:right\">"+rc+"</p>";
			}
		}
		return(rc);
	}
}
