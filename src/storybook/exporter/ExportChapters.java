/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storybook.exporter;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import storybook.model.BookModel;
import storybook.model.EntityUtil;
import storybook.model.hbn.dao.ChapterDAOImpl;
import storybook.model.hbn.entity.Chapter;
import storybook.toolkit.I18N;

/**
 *
 * @author favdb
 */
public class ExportChapters {
	private final Export parent;
	private ExportPDF pdf;
	private ExportHtml html;
	private ExportCsv csv;
	private ExportTxt txt;
	private ExportOdf odf;
	private List<ExportHeader> headers;
	private ExportXml xml;
	private ParamExport paramExport;

	ExportChapters(Export m, ParamExport p) {
		parent=m;
		paramExport=p;
		headers=new ArrayList<>();
		headers.add(new ExportHeader(I18N.getMsg("msg.common.id"),5));
		headers.add(new ExportHeader(I18N.getMsg("msg.dlg.chapter.number"), 10));
		headers.add(new ExportHeader(I18N.getMsg("msg.common.title"), 80));
	}
	
	public String get(Chapter obj) {
		if (obj!=null) return(EntityUtil.getInfo(parent.mainFrame, obj));
		String str = debut(obj);
		BookModel model = parent.mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ChapterDAOImpl dao = new ChapterDAOImpl(session);
		List<Chapter> chapters = dao.findAll();
		for (Chapter chapter : chapters) {
			str += ligne(chapter, true, true);
		}
		model.commit();
		end();
		return(str);
	}
	
	public String debut(Chapter obj) {
		String rep="Chapters";
		switch(parent.format) {
			case "pdf":
				pdf=new ExportPDF(parent,rep,parent.file.getAbsolutePath(),headers);
				pdf.open();
				break;
			case "html":
				html=new ExportHtml(parent,rep,parent.file.getAbsolutePath(),headers,paramExport);
				html.open(true);
				break;
			case "csv":
				csv=new ExportCsv(parent,rep,parent.file.getAbsolutePath(),headers);
				csv.open();
				break;
			case "txt":
				txt=new ExportTxt(parent,rep,parent.file.getAbsolutePath(),headers);
				txt.open();
				break;
			case "odf":
				odf=new ExportOdf(parent,rep,parent.file.getAbsolutePath(),headers);
				odf.open();
				break;
			case "xml":
				xml=new ExportXml(parent,rep,parent.file.getAbsolutePath(),headers);
				xml.open();
				break;
		}
		return ("");
	}
	
	public String ligne(Chapter obj, boolean verbose, boolean list) {
		String body[]={
			Long.toString(obj.getId()),
			Integer.toString(obj.getChapterno()),
			obj.getTitle()
		};
		switch(parent.format) {
			case "pdf":
				pdf.writeRow(body);
				break;
			case "html":
				html.writeRow(body);
				break;
			case "csv":
				csv.writeRow(body);
				break;
			case "txt":
				txt.writeRow(body);
				break;
			case "odf":
				odf.writeRow(body);
				break;
			case "xml":
				xml.writeChapter(obj);
				break;
		}
		return("");
	}
	
	public void end() {
		switch(parent.format) {
			case "html":
				html.close(true);
				break;
			case "pdf":
				pdf.close();
				break;
			case "csv":
				csv.close();
				break;
			case "txt":
				txt.close();
				break;
			case "odf":
				odf.close();
				break;
			case "xml":
				xml.close();
				break;
		}
	}

}
