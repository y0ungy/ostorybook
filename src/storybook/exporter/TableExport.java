/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import storybook.SbApp;
import storybook.SbConstants;
import storybook.model.BookModel;
import storybook.model.hbn.dao.*;
import storybook.model.hbn.entity.*;
import storybook.toolkit.BookUtil;
import storybook.toolkit.I18N;
import storybook.toolkit.TextUtil;
import storybook.ui.MainFrame;
import storybook.ui.SbView;
import storybook.ui.dialog.DlgException;

/**
 *
 * @author favdb
 */
public class TableExport {

	private MainFrame mainFrame;
	private String viewName="";
	private BufferedWriter outStream;
	private boolean isOpened;
	private String dir;
	private String format;
	private boolean isDocBook=false;
	private ParamExport exportParam;
	private String tableTitle;
	
	public TableExport(MainFrame mainFrame, String format) {
		this.mainFrame=mainFrame;
		this.dir=mainFrame.getDbFile().getPath();
		this.format=format;
		exportParam=new ParamExport(mainFrame);
	}

	public TableExport(MainFrame m, SbView view) {
		SbApp.trace("TableExport.Export(mainFrame,"+view.getName()+")");
		mainFrame=m;
		viewName=view.getName();
		dir=mainFrame.getDbFile().getPath();
		exportParam=new ParamExport(mainFrame);
		format=exportParam.format;
		tableTitle=view.getTitle();
		String ret=askFileExists(viewName)+"\n";
		if (!ret.replace("\n", "").isEmpty()) {
			if (JOptionPane.showConfirmDialog(this.mainFrame,
				I18N.getMsg("msg.common.export.replace", ret),
				I18N.getMsg("msg.common.export"),
				JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		if (openFile()) return;
		writeFile(viewName);
		closeFile();
	}
	
	public void setDir(String d) {
		dir=d;
	}
	public void setFormat(String f) {
		format=f;
	}

	public boolean openFile() {
		String fileName=dir+File.separator+viewName+"."+format;
		File file=new File(dir);
		if (!(file.exists() && file.isDirectory())) {
			JOptionPane.showMessageDialog(mainFrame,
					I18N.getMsg("msg.export.dir.error"),
					I18N.getMsg("msg.export"), 1);
			return(true);
		}
		try {
			outStream = new BufferedWriter(new FileWriter(fileName));
			String str = "<?xml version='1.0'?>\n";
			if (isDocBook) {
				str+="<book xmlns=\"http://docbook.org/ns/docbook\" version=\"5.0\">\n";
			}
			else str += "<book>\n";
			outStream.write(str, 0, str.length());
			outStream.flush();
			isOpened = true;
		} catch (IOException ex) {
			DlgException.showDialog("Export", ex);
			return(true);
		}
		return(false);
	}
	public void writeFile(String name) {
		if (isOpened==false) return;
		switch (name) {
			case "Attributes": writeAttribute(); break;
			case "Categories": writeCategory(); break;
			case "Chapters": writeChapter(); break;
			case "Genders": writeGender(); break;
			case "Ideas": writeIdea(); break;
			case "Internals": writeInternal(); break;
			case "Items": writeItem(); break;
			case "ItemLinks": writeItemLink(); break;
			case "Locations": writeLocation(); break;
			case "Memos": writeMemo(); break;
			case "Parts": writePart(); break;
			case "Persons": writePerson(); break;
			case "Relationships": writeRelationship(); break;
			case "Scenes": writeScene(); break;
			case "Strands": writeStrand(); break;
			case "Tags": writeTag(); break;
			case "TagLinks": writeTagLink(); break;
			case "TimeEvents": writeTimeEvent(); break;
			default: break;
		}
	}

	public void closeFile() {
		if (isOpened==false) return;
		try {
			String str = "";
			str += "</book>\n";
			outStream.write(str, 0, str.length());
			outStream.flush();
			outStream.close();
			isOpened = false;
			JOptionPane.showMessageDialog(mainFrame,
				I18N.getMsg("msg.common.export.success") + "\n" + dir + File.separator + tableTitle + "." + format,
				I18N.getMsg("msg.common.export"), JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException ex) {
			SbApp.error("ExportXml.close()", ex);
		}
	}

	private void writeAttribute() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		AttributeDAOImpl dao = new AttributeDAOImpl(session);
		List<Attribute> entities = dao.findAll();
		for (Attribute e: entities) {
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		}
		model.commit();
	}

	private void writeCategory() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		CategoryDAOImpl dao = new CategoryDAOImpl(session);
		List<Category> entities = dao.findAll();
		for (Category e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeChapter() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ChapterDAOImpl dao = new ChapterDAOImpl(session);
		List<Chapter> entities = dao.findAll();
		for (Chapter e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeGender() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		GenderDAOImpl dao = new GenderDAOImpl(session);
		List<Gender> entities = dao.findAll();
		for (Gender e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeIdea() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		IdeaDAOImpl dao = new IdeaDAOImpl(session);
		List<Idea> entities = dao.findAll();
		for (Idea e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeInternal() {
		writeText("    <info>\n");
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		InternalDAOImpl dao = new InternalDAOImpl(session);
		List<Internal> entities = dao.findAll();
		for (Internal e: entities) {
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		}
		model.commit();
		writeText("    </info>\n");
	}

	private void writeItem() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ItemDAOImpl dao = new ItemDAOImpl(session);
		List<Item> entities = dao.findAll();
		for (Item e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeItemLink() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ItemLinkDAOImpl dao = new ItemLinkDAOImpl(session);
		List<ItemLink> entities = dao.findAll();
		for (ItemLink e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeLocation() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		LocationDAOImpl dao = new LocationDAOImpl(session);
		List<Location> entities = dao.findAll();
		for (Location e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeMemo() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		MemoDAOImpl dao = new MemoDAOImpl(session);
		List<Memo> entities = dao.findAll();
		for (Memo e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writePart() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PartDAOImpl dao = new PartDAOImpl(session);
		List<Part> entities = dao.findAll();
		for (Part e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writePerson() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PersonDAOImpl dao = new PersonDAOImpl(session);
		List<Person> entities = dao.findAll();
		for (Person e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeRelationship() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		RelationshipDAOImpl dao = new RelationshipDAOImpl(session);
		List<Relationship> entities = dao.findAll();
		for (Relationship e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeScene() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		SceneDAOImpl dao = new SceneDAOImpl(session);
		List<Scene> entities = dao.findAll();
		for (Scene e: entities) {
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		}
		model.commit();
	}

	private void writeStrand() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		StrandDAOImpl dao = new StrandDAOImpl(session);
		List<Strand> entities = dao.findAll();
		for (Strand e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeTag() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		TagDAOImpl dao = new TagDAOImpl(session);
		List<Tag> entities = dao.findAll();
		for (Tag e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeTagLink() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		TagLinkDAOImpl dao = new TagLinkDAOImpl(session);
		List<TagLink> entities = dao.findAll();
		for (TagLink e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}

	private void writeTimeEvent() {
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		TimeEventDAOImpl dao = new TimeEventDAOImpl(session);
		List<TimeEvent> entities = dao.findAll();
		for (TimeEvent e: entities)
			switch(format) {
				case "xml": writeText(e.toXml());break;
				case "csv": writeText(e.toCsv(exportParam.csvQuote,exportParam.csvComma));break;
			}
		model.commit();
	}
	
	private void writeText(String str) {
		if ("".equals(str)) {
			return;
		}
		SbApp.trace("ExportXml.writeText(" + TextUtil.truncateString(str, 32) + ")");
		try {
			String s = str;
			outStream.write(s, 0, s.length());
			outStream.flush();
		} catch (IOException ex) {
			SbApp.error("ExportXml.writeText(" + str + ")", ex);
		}
	}

	public void DocBook() {
		isDocBook=true;
		viewName=BookUtil.get(mainFrame, SbConstants.BookKey.TITLE, "").getStringValue();
		if (openFile()) return;
		BookModel model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PartDAOImpl PartDAO = new PartDAOImpl(session);
		ChapterDAOImpl ChapterDAO = new ChapterDAOImpl(session);
		SceneDAOImpl SceneDAO = new SceneDAOImpl(session);
		List<Part> listParts = PartDAO.findAll();
		for (Part part : listParts) {
			writeText("<part>\n<title>" + part.getName() + "</title>\n");
			List<Chapter> chapters = ChapterDAO.findAll(part);
			for (Chapter chapter : chapters) {
				writeText("<chapter>\n<title>" + chapter.getTitle() + "</title>\n");
				List<Scene> scenes = SceneDAO.findByChapter(chapter);
				for (Scene scene : scenes) {
					writeText("<para>\n" + scene.getSummary()+ "</para>\n");
				}
				writeText("</chapter>\n");
			}
			writeText("</part>\n");
		}
		closeFile();
	}

	public void DataBase() {
		viewName=BookUtil.get(mainFrame, SbConstants.BookKey.TITLE, "").getStringValue();
		viewName="DataBase";
		if (viewName=="") {
			JOptionPane.showMessageDialog(mainFrame,
					I18N.getMsg("msg.export.title.error"),
					I18N.getMsg("msg.export"), 1);
			return;
		}
		if (openFile()) return;
		writeInternal();
		writeAttribute();
		writeCategory();
		writeGender();
		writeItem();
		writeItemLink();
		writeLocation();
		writePerson();
		writeRelationship();
		writeStrand();
		writeTag();
		writeTagLink();
		writeIdea();
		writeMemo();
		writeRelationship();
		writeTimeEvent();
		writePart();
		writeChapter();
		writeScene();
		tableTitle=I18N.getMsg("msg.file.export.basexml");
		closeFile();
	}

	private void getPart(Part part) {
		writeText("<part>\n<title>" + part.getName() + "</title>\n");
	}

	private void getChapter(Chapter chapter) {
		writeText("<chapter>\n<title>" + chapter.getTitle() + "</title>\n");
	}

	private void getScene(Scene scene) {
		writeText("<para>\n" + scene.getSummary() + "</para>\n");
	}

	private void endChapter() {
		writeText("</chapter>\n");
	}

	private void endPart() {
		writeText("</part>\n");
	}

	public String askFileExists(String name) {
		String fileName=dir+File.separator+name+"."+format;
		File file=new File(fileName);
		if (file.exists()) return(fileName);
		return("");
	}

}
