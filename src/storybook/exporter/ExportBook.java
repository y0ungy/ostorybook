/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package storybook.exporter;

import java.io.File;
import java.util.List;
import org.hibernate.Session;
import storybook.SbConstants;
import storybook.SbApp;
import storybook.model.BookModel;
import storybook.model.hbn.dao.ChapterDAOImpl;
import storybook.model.hbn.dao.PartDAOImpl;
import storybook.model.hbn.dao.SceneDAOImpl;
import storybook.model.hbn.entity.Chapter;
import storybook.model.hbn.entity.Part;
import storybook.model.hbn.entity.Scene;
import storybook.toolkit.BookUtil;
import storybook.toolkit.I18N;
import storybook.toolkit.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class ExportBook {

	private final Export parent;
	private ExportHtml html;
	private ExportPDF pdf;
	private ExportCsv csv;
	private ExportTxt txt;
	private ExportOdf odf;
	private final BookExporter bookExporter;
	private boolean isMultiHtml;
	private String baseFilename;
	private ExportXml xml;
	private Chapter firstChapter=null;
	private Chapter lastChapter=null;
	private Chapter priorChapter=null;
	private Chapter nextChapter=null;
	private String title;
	private String subtitle;
	private String copyright;
	private final ParamExport paramExport;

	public ExportBook(Export m, ParamExport p) {
		parent = m;
		bookExporter = new BookExporter(m.mainFrame);
		baseFilename = parent.directory + File.separator;
		baseFilename += BookUtil.get(parent.mainFrame, SbConstants.BookKey.TITLE, "").getStringValue();
		paramExport=p;
		isMultiHtml = p.htmlBookMulti;
	}

	public String get() {
		SbApp.trace("ExportBook.get()");
		String rc = "";
		title=BookUtil.get(parent.mainFrame, SbConstants.BookKey.TITLE, "").getStringValue();
		subtitle=BookUtil.get(parent.mainFrame, SbConstants.BookKey.SUBTITLE).getStringValue();
		copyright=BookUtil.get(parent.mainFrame, SbConstants.BookKey.COPYRIGHT).getStringValue();
		BookModel model = parent.mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PartDAOImpl PartDAO = new PartDAOImpl(session);
		List<Part> parts = PartDAO.findAll();
		ChapterDAOImpl ChapterDAO = new ChapterDAOImpl(session);
		List<Chapter> chapters = ChapterDAO.findAllOrderByChapterNoAndSceneNo();
		firstChapter=chapters.get(0);
		lastChapter=chapters.get(chapters.size()-1);
		if (parent.format.equals("html") && isMultiHtml) {
			html = new ExportHtml(parent, "Book", baseFilename + " index.html", null, paramExport);
			html.open(false);
			getTitle();
			getTOC(parts, chapters);
			getNavBar(null,null,firstChapter);
			html.close(false);
		}
		switch (parent.format) {
			case "html":
				html = new ExportHtml(parent, "Book", baseFilename + ".html", null, paramExport);
				break;
			case "csv":
				break; // no export csv Book
			case "txt":
				txt = new ExportTxt(parent, "Book", baseFilename + ".txt", null);
				bookExporter.tHtml=false;
				break;
			case "pdf":
				html = new ExportHtml(parent, "Book", baseFilename + "workingFile.html", null, paramExport);
				isMultiHtml = false;
				break;
			case "odf":
				odf = new ExportOdf(parent, "Book", baseFilename + ".odt", null);
				break;
			case "xml":
				xml = new ExportXml(parent, "Book", baseFilename + ".xml", null);
				break;
		}
		if (!isMultiHtml) {
			rc = debut();
			if (!"".equals(rc)) {
				return (rc);
			}
			getTitle();
			getTOC(parts,chapters);
		}
		SceneDAOImpl SceneDAO = new SceneDAOImpl(session);
		for (Part part : parts) {
			getPart(part);
			for (int i=0;i<chapters.size();i++) {
				Chapter chapter=chapters.get(i);
				if (!chapter.getPart().equals(part)) continue;
				getChapter(chapter, ChapterDAO);
				List<Scene> scenes = SceneDAO.findByChapter(chapter);
				for (Scene scene : scenes) {
					getScene(scene);
				}
				if ("xml".equals(parent.format)) {
					xml.endChapter();
				}
				if (isMultiHtml) {
					Chapter precedent=null;
					if (i>0) precedent=chapters.get(i-1);
					Chapter suivant=null;
					if (i<chapters.size()-1) suivant=chapters.get(i+1);
					getNavBar(precedent,chapter,suivant);					
				}
			}
			if ("xml".equals(parent.format)) {
				xml.endPart();
			}
		}
		fin();
		//model.commit();
		return (rc);
	}

	public String debut() {
		SbApp.trace("ExportBook.debut()");
		String rc = "";
		switch (parent.format) {
			case "html":
				html.open(false);
				break;
			case "csv":
				csv.open();
				break;//no header
			case "txt":
				txt.open();
				break;//no header
			case "pdf":
				html.open(false);
				break;
			case "odf":
				odf.open();
				break;
			case "xml":
				xml.open();
				break;
		}
		return (rc);
	}

	private void getPart(Part part) {
		SbApp.trace("ExportBook.getPart(" + part.getName() + ")");
		switch (parent.format) {
			case "html":
				if (!isMultiHtml) {
					html.writeText(bookExporter.getPartAsHtml(part), false);
				} // else no export for the Part
				break;
			case "csv":
				break; // no csv export for book
			case "txt":
				txt.writeText(bookExporter.getPartAsTxt(part));
				break;
			case "pdf":
				html.writeText(bookExporter.getPartAsHtml(part), false);
				break;
			case "odf":
				odf.writePart(bookExporter.getPartAsTxt(part));
				break;
			case "xml":
				xml.writePart(bookExporter.getPartAsTxt(part));
				break;
		}
	}

	private String getChapterId(Chapter chapter) {
		String spart = Integer.toString(chapter.getPart().getNumber());
		String schapter = Integer.toString(chapter.getChapterno());
		if (spart.length() < 2) {
			spart = "0" + spart;
		}
		if (schapter.length() < 2) {
			schapter = "0" + schapter;
		}
		return (spart + "-" + schapter);
	}

	private void getChapter(Chapter chapter, ChapterDAOImpl ChapterDAO) {
		SbApp.trace("ExportBook.getChapter(" + chapter.getTitle() + ", dao)");
		switch (parent.format) {
			case "html":
				if (isMultiHtml) {
					if (html.isOpened) {
						html.close(false);
					}
					html = new ExportHtml(parent, "Book",
							baseFilename + " " + getChapterId(chapter) + ".html",
							null, /*parent.author, */paramExport);
					html.open(false);
				}
				html.writeText(bookExporter.getChapterAsHtml(chapter, ChapterDAO), false);
				break;
			case "csv":
				break; // no export csv Book
			case "txt":
				txt.writeText(bookExporter.getChapterAsTxt(chapter, ChapterDAO));
				break;
			case "pdf":
				html.writeText(bookExporter.getChapterAsHtml(chapter, ChapterDAO), false);
				break;
			case "odf":
				odf.writeChapter(bookExporter.getChapterAsTxt(chapter, ChapterDAO));
				break;
			case "xml":
				xml.writeChapter(bookExporter.getChapterAsTxt(chapter, ChapterDAO));
				break;
		}
	}

	private void getScene(Scene scene) {
		SbApp.trace("ExportBook.getScene(" + scene.getFullTitle() + ")");
		switch (parent.format) {
			case "html":
				html.writeText(bookExporter.getSceneAsHtml(scene,parent.directory), false);
				break;
			case "csv":
				break; // no export csv Book
			case "txt":
				txt.writeText(bookExporter.getSceneAsTxt(scene));
				break;
			case "pdf":
				html.writeText(bookExporter.getSceneAsHtml(scene), false);
				break;
			case "odf":
				odf.writeScene(bookExporter.getSceneAsHtml(scene));
				break;
			case "xml":
				xml.writeScene(bookExporter.getSceneAsTxt(scene));
				break;
		}
	}

	private void fin() {
		SbApp.trace("ExportBook.fin()");
		switch (parent.format) {
			case "html":
				html.close(false);
				break;
			case "pdf":
				html.close(false);
				pdf = new ExportPDF(parent, "Book", baseFilename + ".pdf", null);
				pdf.open();
				pdf.HtmlToPdf(baseFilename + "workingFile.html");
				pdf.close();
				break;
			case "csv":
				csv.close();
				break;
			case "txt":
				txt.close();
				break;
			case "odf":
				odf.close();
				break;
			case "xml":
				xml.close();
				break;
		}
	}

	private void getTocPart(Part part) {
		writeText("<h2>" + part.getNumberName() + "</h2>");
	}

	private void getTOCChapter(Chapter chapter) {
		StringBuilder str = new StringBuilder("<a href=\"");
		if (isMultiHtml)
			str.append(title).append(" ")
				.append(getChapterId(chapter)).append(".html\">");
		else 
			str.append("#").append(chapter.getChapterno()).append("\">");
		str.append(chapter.getChapternoStr()).append(" ").append(chapter.getTitle()).append("</a><br>\n");
		writeText(str.toString());
	}

	private void getNavBar(Chapter precedent, Chapter current, Chapter suivant) {
		//sommaire
		String toc=" ";
		String imgSum=I18N.getMsg("msg.export.nav.summary");
		String imgFirst=I18N.getMsg("msg.export.nav.first");
		String imgNext=I18N.getMsg("msg.export.nav.next");
		String imgPrior=I18N.getMsg("msg.export.nav.prior");
		String imgLast=I18N.getMsg("msg.export.nav.last");
		if (paramExport.htmlNavImage) {
			imgSum=setNavImage("summary",imgSum);
			imgFirst=setNavImage("first",imgFirst);
			imgNext=setNavImage("next",imgNext);
			imgPrior=setNavImage("previous",imgPrior);
			imgLast=setNavImage("last",imgLast);
		}
		if (current!=null) {
			toc="<a href=\""
				+ title
				+ " index.html" + "\">"
				+ imgSum  + "</a> | ";
		}
		//premier chapitre
		String first = " ";
		if (precedent!=null) {
			first="<a href=\""
				+ title + " " + getChapterId(firstChapter) + ".html\">"
				+ imgFirst  + "</a> | ";
		}
		//chapitre precedent
		String prior = "";
		if (precedent!=null) {
			prior="<a href=\""
				+ title + " " + getChapterId(precedent) + ".html\">"
				+ imgPrior  + "</a> | ";
		}
		//chapitre suivant
		String next="";
		if (suivant!=null) {
			next="<a href=\""
				+ title + " " + getChapterId(suivant) + ".html\">"
				+ imgNext  + "</a>";
		}
		//dernier chapitre
		String last= "";
		if (suivant!=null) {
			last=" | <a href=\""
				+ title + " " + getChapterId(lastChapter) + ".html\">"
				+ imgLast  + "</a></p>\n";
		}
		String str=first+prior+toc+next+last;
		html.writeText(str, true);
	}

	private void writeText(String str) {
		switch (parent.format) {
			case "html":
				html.writeText(str, false);
				break;
			case "txt":
				txt.writeText(HtmlUtil.htmlToText(str));
				break;
		}
	}
	
	private void getTitle() {
		SbApp.trace("ExportBook.getTitle()");
		String str = "<h1>" + HtmlUtil.textToHtmlCode(parent.bookTitle) + "</h1>";
		if (!subtitle.isEmpty()) str+="<h2>"+HtmlUtil.textToHtmlCode(subtitle)+"</h2>";
		if (!parent.author.isEmpty()) str+="<p>par "+HtmlUtil.textToHtmlCode(parent.author)+"<br>";
		if (!copyright.isEmpty())
			str+="<i>© "+HtmlUtil.textToHtmlCode(copyright)+"</i><br>";
		str+="<i><small>"+
			HtmlUtil.textToHtmlCode(I18N.getMsg("msg.export.by")+ " " +
			SbConstants.Storybook.PRODUCT_FULL_NAME)+"</small></i></p>";
		writeText(str);
	}

	private void getTOC(List<Part> parts, List<Chapter> chapters) {
		String str="<h2>"+I18N.getMsg("msg.export.toc")+"</h2>";
		writeText(str);
		if (parts==null) return;
		for (Part part : parts) {
			if (parts.size()>1) getTocPart(part);
			for (Chapter chapter:chapters) {
				if (chapter==null) continue;
				if (!chapter.getPart().equals(part)) continue;
				getTOCChapter(chapter);
			}
		}
	}

	String askFileExists() {
		System.out.println("ExportBook.askFileExists("+baseFilename+"."+parent.format+")");
		String ret="";
		ExportHtml h;
		if (isMultiHtml) {
			h=new ExportHtml(parent, "Book", baseFilename + " index.html", null, /*parent.author, */paramExport);
		} else {
			h=new ExportHtml(parent, "Book", baseFilename + "."+parent.format, null, /*parent.author, */paramExport);
		}
		File f=new File(h.fileName);
		if (f.exists()) ret=f.getAbsolutePath();
		return(ret);
	}

	private String setNavImage(String img,String name) {
		//TODO add copy file from ressources to img directory
		return("<img src=\"img/"+img+".png\" title=\""+name+"\" alt=\""+name+"\">");
	}

}
