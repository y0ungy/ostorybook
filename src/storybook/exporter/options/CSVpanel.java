/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.exporter.options;

import storybook.exporter.ParamExport;

/**
 *
 * @author favdb
 */
public class CSVpanel extends javax.swing.JPanel {

	private ParamExport param;

	/**
	 * Creates new form CSVpanel
	 */
	public CSVpanel() {
		initComponents();
	}

	public CSVpanel(ParamExport param) {
		this.param=param;
		initComponents();
		initUI();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        group1 = new javax.swing.ButtonGroup();
        group2 = new javax.swing.ButtonGroup();
        jLabel2 = new javax.swing.JLabel();
        csvSingleQuotes = new javax.swing.JRadioButton();
        csvDoubleQuotes = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        csvComma = new javax.swing.JRadioButton();
        csvSemicolon = new javax.swing.JRadioButton();
        csvNoQuotes = new javax.swing.JRadioButton();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("storybook/msg/messages"); // NOI18N
        jLabel2.setText(bundle.getString("msg.export.options.csv.quoted")); // NOI18N

        group1.add(csvSingleQuotes);
        csvSingleQuotes.setText(bundle.getString("msg.export.options.csv.quoted.single")); // NOI18N

        group1.add(csvDoubleQuotes);
        csvDoubleQuotes.setText(bundle.getString("msg.export.options.csv.quoted.double")); // NOI18N

        jLabel3.setText(bundle.getString("msg.export.options.csv.separate")); // NOI18N

        group2.add(csvComma);
        csvComma.setText(bundle.getString("msg.export.options.csv.separate.comma")); // NOI18N

        group2.add(csvSemicolon);
        csvSemicolon.setText(bundle.getString("msg.export.options.csv.separate.semicolon")); // NOI18N

        group1.add(csvNoQuotes);
        csvNoQuotes.setText(bundle.getString("msg.common.none")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(csvSingleQuotes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(csvDoubleQuotes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(csvNoQuotes))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(csvComma)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(csvSemicolon)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(csvSingleQuotes)
                    .addComponent(csvDoubleQuotes)
                    .addComponent(csvNoQuotes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(csvComma)
                    .addComponent(csvSemicolon))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JRadioButton csvComma;
    public javax.swing.JRadioButton csvDoubleQuotes;
    public javax.swing.JRadioButton csvNoQuotes;
    public javax.swing.JRadioButton csvSemicolon;
    public javax.swing.JRadioButton csvSingleQuotes;
    private javax.swing.ButtonGroup group1;
    private javax.swing.ButtonGroup group2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables

	private void initUI() {
		if (null!=param.csvQuote) switch (param.csvQuote) {
			case "'":
				csvSingleQuotes.setSelected(true);
				break;
			case "\"":
				csvDoubleQuotes.setSelected(true);
				break;
			default:
				csvNoQuotes.setSelected(true);
				break;
		}
		if (";".equals(param.csvComma)) csvSemicolon.setSelected(true);
		else csvComma.setSelected(true);
	}
}
