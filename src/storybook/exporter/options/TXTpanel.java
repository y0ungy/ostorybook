/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.exporter.options;

import java.awt.event.ItemEvent;
import storybook.exporter.ParamExport;

/**
 *
 * @author favdb
 */
public class TXTpanel extends javax.swing.JPanel {

	private ParamExport param;

	/**
	 * Creates new form TXTpanel
	 */
	public TXTpanel() {
		initComponents();
	}

	public TXTpanel(ParamExport param) {
		this.param=param;
		initComponents();
		initUI();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtTab = new javax.swing.JRadioButton();
        txtOther = new javax.swing.JRadioButton();
        txtSeparator = new javax.swing.JTextField();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("storybook/msg/messages"); // NOI18N
        jLabel1.setText(bundle.getString("msg.export.options.csv.separate")); // NOI18N

        txtTab.setText("tab");

        txtOther.setText(bundle.getString("msg.common.other")); // NOI18N
        txtOther.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                txtOtherItemStateChanged(evt);
            }
        });

        txtSeparator.setAutoscrolls(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTab)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtOther)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtTab)
                    .addComponent(txtOther)
                    .addComponent(txtSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtOtherItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_txtOtherItemStateChanged
        if (evt.getStateChange()==ItemEvent.SELECTED) {
            txtSeparator.setVisible(true);
        } else {
            txtSeparator.setVisible(false);
        }
    }//GEN-LAST:event_txtOtherItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    public javax.swing.JRadioButton txtOther;
    public javax.swing.JTextField txtSeparator;
    public javax.swing.JRadioButton txtTab;
    // End of variables declaration//GEN-END:variables

	private void initUI() {
		txtTab.setSelected(param.txtTab);
		if (!param.txtTab) {
			txtSeparator.setText(param.txtSeparator);
			txtSeparator.setVisible(true);
		} else {
			txtSeparator.setVisible(false);
		}

	}
}
