/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storybook.exporter;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import storybook.SbApp;
import storybook.model.BookModel;
import storybook.model.EntityUtil;
import storybook.model.hbn.dao.PartDAOImpl;
import storybook.model.hbn.entity.Part;
import storybook.toolkit.I18N;

/**
 *
 * @author favdb
 */
public class ExportParts {
	private final Export parent;
	private ExportPDF pdf;
	private ExportHtml html;
	private ExportCsv csv;
	private ExportTxt txt;
	private ExportOdf odf;
	private List<ExportHeader> headers;
	private ExportXml xml;
	private final ParamExport paramExport;
	
	ExportParts(Export m, ParamExport p) {
		SbApp.trace("ExportParts.get("+m.toString()+")");
		parent=m;
		paramExport=p;
		headers=new ArrayList<>();
		headers.add(new ExportHeader(I18N.getMsg("msg.common.id"),5));
		headers.add(new ExportHeader(I18N.getMsg("msg.dlg.part.number"), 5));
		headers.add(new ExportHeader(I18N.getMsg("msg.dlg.part.name"), 90));
	}
	
	public String get(Part obj) {
		SbApp.trace("ExportParts.get("+(obj!=null?obj.getName():"null")+")");
		if (obj!=null) return(EntityUtil.getInfo(parent.mainFrame, obj));
		String str = debut(obj);
		BookModel model = parent.mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PartDAOImpl dao = new PartDAOImpl(session);
		List<Part> parts = dao.findAll();
		for (Part part : parts) {
			str += ligne(part, true, true);
		}
		model.commit();
		end();
		return(str);
	}
	
	public String debut(Part obj) {
		SbApp.trace("ExportParts.debut("+(obj!=null?obj.getName():"null")+")");
		String rep="Parts";
		switch(parent.format) {
			case "pdf":
				pdf=new ExportPDF(parent,rep,parent.file.getAbsolutePath(),headers);
				pdf.open();
				break;
			case "html":
				html=new ExportHtml(parent,rep,parent.file.getAbsolutePath(),headers,paramExport);
				html.open(true);
				break;
			case "csv":
				csv=new ExportCsv(parent,rep,parent.file.getAbsolutePath(),headers);
				csv.open();
				break;
			case "txt":
				txt=new ExportTxt(parent,rep,parent.file.getAbsolutePath(),headers);
				txt.open();
				break;
			case "odf":
				odf=new ExportOdf(parent,rep,parent.file.getAbsolutePath(),headers);
				odf.open();
				break;
			case "xml":
				xml=new ExportXml(parent,rep,parent.file.getAbsolutePath(),headers);
				xml.open();
				break;
		}
		return ("");
	}
	
	public String ligne(Part part, boolean verbose, boolean list) {
		SbApp.trace("ExportParts.get("
			+(part!=null?part.getName():"null")
			+","+(verbose?"verbose":"noverbose")
			+","+(list?"list":"nolist")+")");
		String body[]={
			Long.toString(part.getId()),
			Integer.toString(part.getNumber()),
			part.getName()
		};
		switch(parent.format) {
			case "pdf":
				pdf.writeRow(body);
				break;
			case "html":
				html.writeRow(body);
				break;
			case "csv":
				csv.writeRow(body);
				break;
			case "txt":
				break;
			case "xml":
				xml.writePart(part);
				break;
		}
		return("");
	}

	public void end() {
		SbApp.trace("ExportParts.end()");
		switch(parent.format) {
			case "html":
				html.close(true);
				break;
			case "pdf":
				pdf.close();
				break;
			case "csv":
				csv.close();
				break;
			case "txt":
				txt.close();
				break;
			case "odf":
				odf.close();
				break;
			case "xml":
				xml.close();
				break;
		}
	}

}
