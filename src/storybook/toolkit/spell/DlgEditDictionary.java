/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.toolkit.spell;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import jortho.SpellChecker;
import jortho.UserDictionaryProvider;
import storybook.toolkit.I18N;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class DlgEditDictionary extends javax.swing.JDialog {

	private boolean isModify=false;

	/*
	 * Creates new form DlgEditDictionary
	 */
	public DlgEditDictionary(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		this.setLocationRelativeTo(parent);
		initUI();
	}

	public DlgEditDictionary(MainFrame parent) {
		super(parent, true);
		initComponents();
		this.setLocationRelativeTo(parent);
		initUI();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txWord = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        btSearch = new javax.swing.JButton();
        btRemove = new javax.swing.JButton();
        btExit = new javax.swing.JButton();
        btAdd = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("storybook/msg/messages"); // NOI18N
        jLabel1.setText(bundle.getString("msg.search.for")); // NOI18N

        txWord.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txWordCaretUpdate(evt);
            }
        });

        jLabel2.setText(bundle.getString("msg.dict.user")); // NOI18N

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        btSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/storybook/resources/icons/16x16/search.png"))); // NOI18N
        btSearch.setToolTipText(bundle.getString("jortho.searchword")); // NOI18N
        btSearch.setEnabled(false);
        btSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSearchActionPerformed(evt);
            }
        });

        btRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/storybook/resources/icons/16x16/minus.png"))); // NOI18N
        btRemove.setToolTipText(bundle.getString("jortho.delete")); // NOI18N
        btRemove.setEnabled(false);
        btRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRemoveActionPerformed(evt);
            }
        });

        btExit.setText(bundle.getString("msg.common.exit")); // NOI18N
        btExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExitActionPerformed(evt);
            }
        });

        btAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/storybook/resources/icons/16x16/plus.png"))); // NOI18N
        btAdd.setToolTipText(bundle.getString("jortho.addToDictionary")); // NOI18N
        btAdd.setEnabled(false);
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txWord, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btAdd))
                    .addComponent(btRemove))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btExit, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btSearch))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2))
                    .addComponent(btAdd))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addComponent(btExit))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btRemove)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExitActionPerformed
		dispose();
    }//GEN-LAST:event_btExitActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
		btRemove.setEnabled(true);
    }//GEN-LAST:event_jList1ValueChanged

    private void btSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSearchActionPerformed
		String tx=txWord.getText();
		if (tx.isEmpty()) return;
		ListModel model=jList1.getModel();
		for( int i=0; i<model.getSize(); i++){
			if (model.getElementAt(i).toString().equals(tx)) {
				jList1.setSelectedValue(model.getElementAt(i).toString(),true);
				break;				
			}
		}
    }//GEN-LAST:event_btSearchActionPerformed

	@SuppressWarnings("unchecked")
    private void btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddActionPerformed
		String tx=txWord.getText();
		if (tx.isEmpty()) return;
		if(tx.contains(" ")) {
			errorMessage(I18N.getMsg("jortho.word.nospace"));
			return;
		}
		if (wordExists(tx)) {
			jList1.setSelectedValue(tx, true);
		}
		((DefaultListModel) jList1.getModel()).addElement(tx);
		ArrayList<String> l = new ArrayList();
		ListModel model = jList1.getModel();
		for( int i=0; i<model.getSize(); i++){
			l.add((String) model.getElementAt(i));
		}
		Collections.sort(l, Collator.getInstance());
		DefaultListModel data = new DefaultListModel();
		for (String str : l) {
			data.addElement(str);
		}
		jList1.setModel(data);
		jList1.setSelectedValue(tx, true);
		isModify=true;
    }//GEN-LAST:event_btAddActionPerformed

    private void btRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRemoveActionPerformed
		int[] selected = jList1.getSelectedIndices();
		Arrays.sort(selected);
		for (int i = selected.length - 1; i >= 0; i--) {
			((DefaultListModel) jList1.getModel()).remove(selected[i]);
			isModify = true;
			btRemove.setEnabled(false);
		}
    }//GEN-LAST:event_btRemoveActionPerformed

    private void txWordCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txWordCaretUpdate
		if (txWord.getText().isEmpty()) {
			btSearch.setEnabled(false);
			btAdd.setEnabled(false);
		} else {
			btSearch.setEnabled(true);
			btAdd.setEnabled(true);
		}
    }//GEN-LAST:event_txWordCaretUpdate

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(DlgEditDictionary.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>
		
		//</editor-fold>

		/* Create and display the dialog */
		java.awt.EventQueue.invokeLater(() -> {
			DlgEditDictionary dialog = new DlgEditDictionary(new javax.swing.JFrame(), true);
			dialog.addWindowListener(new java.awt.event.WindowAdapter() {
				@Override
				public void windowClosing(java.awt.event.WindowEvent e) {
					System.exit(0);
				}
			});
			dialog.setVisible(true);
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btExit;
    private javax.swing.JButton btRemove;
    private javax.swing.JButton btSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<String> jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txWord;
    // End of variables declaration//GEN-END:variables

	private void initUI() {
		loadWordList();
	}

	@SuppressWarnings("unchecked")
	private void loadWordList() {
		DefaultListModel data = new DefaultListModel();
		UserDictionaryProvider provider = SpellChecker.getUserDictionaryProvider();
		if (provider != null) {
			Iterator<String> userWords = provider.getWords(SpellChecker.getCurrentLocale());
			if (userWords != null) {
				ArrayList<String> wordList = new ArrayList<>();
				while (userWords.hasNext()) {
					String word = userWords.next();
					if (word != null && word.length() > 1) {
						wordList.add(word);
					}
				}
				// Liste alphabetical sorting with the user language
				Collections.sort(wordList, Collator.getInstance());
				for (String str : wordList) {
					data.addElement(str);
				}
			}
		}
		jList1.setModel(data);
	}

	@Override
    public void dispose(){
        super.dispose();
        if( isModify ) {
            // save the user dictionary
            UserDictionaryProvider provider = SpellChecker.getUserDictionaryProvider();
            if( provider != null ) {
                ListModel model = jList1.getModel();
                StringBuilder builder = new StringBuilder();
                for( int i=0; i<model.getSize(); i++){
                    if( builder.length() != 0 ){
                        builder.append( '\n' );
                    }
                    builder.append( model.getElementAt(i) );
                }
                provider.setUserWords( builder.toString() );
            }
            // reload the dictionary
            /*JMenu menu = SpellChecker.createLanguagesMenu( null );
            Component[] comps = menu.getMenuComponents();
            for( Component comp : comps ) {
                if( comp instanceof JRadioButtonMenuItem ){
                    JRadioButtonMenuItem item = (JRadioButtonMenuItem)comp;
                    if( item.isSelected() ){
                        item.doClick();
                    }
                }
            }*/
        }
    }

	private boolean wordExists(String tx) {
		ListModel model=jList1.getModel();
		for( int i=0; i<model.getSize(); i++){
			if (model.getElementAt(i).toString().equals(tx)) {
				jList1.setSelectedValue(model.getElementAt(i).toString(),true);
				return(true);				
			}
		}
		return(false);
	}
	private void errorMessage(String s) {
		JOptionPane.showMessageDialog(this,
			I18N.getMsg(s),
			I18N.getMsg("msg.common.error"), JOptionPane.ERROR_MESSAGE);
	}


}
