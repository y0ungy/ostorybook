package storybook.toolkit.filefilter;

import java.io.File;

public class SBFileFilter extends javax.swing.filechooser.FileFilter {

	private final String extension;
	private final String description;
	
	public SBFileFilter(String ext, String desc) {
		extension=ext;
		description=desc;
	}
	@Override
    public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
        String filename = file.getName();
        return filename.endsWith(extension);
    }
	@Override
    public String getDescription() {
        return description;
    }
}
