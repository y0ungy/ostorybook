/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.toolkit.filefilter;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author favdb
 */
public class FileTypeFilter extends FileFilter {
    private String extension;
    private String[] extensions;
    private String description;
 
    public FileTypeFilter(String extension, String description) {
        this.extension = extension;
        this.description = description;
    }
 
    public FileTypeFilter(String[] extensions, String description) {
        this.extensions = extensions;
        this.description = description;
    }
 
	@Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
		if (extension==null) {
			for (String x:extensions) {
				if (file.getName().endsWith(x)) return(true);
			}
			return(false);
		}
        return file.getName().endsWith(extension);
    }
 
	@Override
    public String getDescription() {
		if (extension==null) {
			String d="";
			for (String x:extensions) d+="*"+x+" ,";
			return(description+" ("+d.substring(0,d.lastIndexOf(" ,"))+")");
		}
        return description + String.format(" (*%s)", extension);
    }
}
