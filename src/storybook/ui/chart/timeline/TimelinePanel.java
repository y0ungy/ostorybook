/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.ui.chart.timeline;

import java.awt.Dimension;
import java.awt.Graphics;
import storybook.toolkit.I18N;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class TimelinePanel extends javax.swing.JPanel {

	MainFrame mainFrame;
	private final String titre;
	private Timeline timeline;
	private String titreX="Xtitre";
	private String titreY="Ytitre";
	private Dataset dataset;

	/**
	 * Creates new form TimelinePanel
	 * @param m
	 * @param titre
	 * @param dataset
	 * @param tX
	 * @param tY
	 */
	public TimelinePanel(MainFrame m, String titre, String tX, String tY, Dataset dataset) {
		setPreferredSize(new Dimension(800, 500));
		initComponents();
		this.mainFrame = m;
		this.titre = titre;
		this.titreX=tX;
		this.titreY=tY;
		this.dataset=dataset;
		initUI();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
	 * content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTitre = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbXaxis = new javax.swing.JLabel();
        lbYaxis = new javax.swing.JLabel();

        setBackground(java.awt.SystemColor.text);

        jTitre.setFont(new java.awt.Font("Noto Sans", 1, 18)); // NOI18N
        jTitre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jTitre.setText("jLabel1");

        jPanel1.setBorder(null);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        lbXaxis.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbXaxis.setText("jLabel2");

        lbYaxis.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbYaxis.setText("jLabel2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTitre, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
            .addComponent(lbXaxis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(lbYaxis, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTitre)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbXaxis)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbYaxis, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jTitre;
    private javax.swing.JLabel lbXaxis;
    private javax.swing.JLabel lbYaxis;
    // End of variables declaration//GEN-END:variables
	private void initUI() {
		if ("date".equals(titreX)) {
			lbXaxis.setText(I18N.getMsg("msg.common."+titreX));
			lbYaxis.setText(I18N.getMsg("msg.common."+titreY));
		} else {
			lbXaxis.setText(" ");
			lbYaxis.setText(" ");
		}
		jTitre.setText(titre);
		timeline = new Timeline(mainFrame, titreX, dataset);
		jPanel1.add(timeline);
		lbYaxis.setUI(new VerticalLabelUI());
	}
	
	@Override
	public void paintComponent(Graphics g) {
		timeline.setSize(jPanel1.getWidth(),jPanel1.getHeight());
		timeline.redraw();
	}
}
